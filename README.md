# tuto installation valheim plus

Bonjour jeune lecteur,

Ceci petit tuto te permettra d'installer valheim plus sur ton pc afin d'éviter les problèmes de compatibilités avec les serveur disposant de Valheim+

## Etape 1 : Installer Valheim+ 

https://github.com/valheimPlus/ValheimPlus/releases/tag/0.9.5.5

Premièrement grâce a ce liens tu pourra télécharger le fichier valheim +.
Attention a bien télécharger WindowsClient.zip

![etape 1 telecharger client](images/telechargerclient.PNG)

## Etape 2 : Déplacer le contenu

Après le téléchargement du fichier, ouvres celui-ci grâce 7zip ou Winrar (à ta préférence)

![etape 2 ouvrir et coller](images/7zipfile.PNG)

Copies le contenu et colles le dans votre fichier Valheim qui se trouve dans : 

```
steam\steamapps\common\Valheim
```

Tu peux également trouver ce fichier sur steam en faisant clic droit sur ton jeu -> gérer -> parcourir les fichiers locaux


Voila ton jeu est prêt a être lancé.

Si dans le futur un problème de compatibilité revient.
Refait ce tuto avec la nouvelle version de valheim+ (Je m'occuperai de mettre a jour le lien de téléchargement)

Bon jeu !
